const umlautMap = {
    '\u00dc': 'UE',
    '\u00c4': 'AE',
    '\u00d6': 'OE',
    '\u00fc': 'ue',
    '\u00e4': 'ae',
    '\u00f6': 'oe',
    '\u00df': 'ss',
};


function getDOMElements() {
    var jiraTicket = {};
    jiraTicket.projectName = document.getElementById("project-name-val").innerHTML;
    jiraTicket.ticketKey = document.getElementById("key-val").innerHTML;
    jiraTicket.ticketType = document.getElementById("type-val").innerHTML;
    jiraTicket.ticketSum = document.getElementById("summary-val").innerHTML;
    return jiraTicket;
}

function checkJira(jiraTicket) {
    return Object.keys(jiraTicket).length !== 0 && jiraTicket.constructor === Object &&
        jiraTicket.projectName !== undefined && jiraTicket.projectName !== "" &&
        jiraTicket.ticketSum !== undefined && jiraTicket.ticketSum !== "" &&
        jiraTicket.ticketType !== undefined && jiraTicket.ticketType !== "" &&
        jiraTicket.ticketSum !== undefined && jiraTicket.ticketSum !== ""
}

function jiraTicketTypeValue(type) {
    switch (type) {
        case "bug":
            return "bugfix";
        case "task":
            return "feature";
        case "subtask":
            return "feature";
        case "epic":
            return "feature";
        case "story":
            return "feature";
        default:
            break;
    }
}

function onOpenPlugin() {
    chrome.tabs.executeScript({
        code: '(' + getDOMElements + ')();'
    }, (results) => {
        console.log(results[0]);
        formatJiraTicket(results[0]);
    });
}

onOpenPlugin();

function replaceUmlaute(str) {
    return str
        .replace(/[\u00dc|\u00c4|\u00d6][a-z]/g, (a) => {
            const big = umlautMap[a.slice(0, 1)];
            return big.charAt(0) + big.charAt(1).toLowerCase() + a.slice(1);
        })
        .replace(new RegExp('[' + Object.keys(umlautMap).join('|') + ']', "g"),
            (a) => umlautMap[a])
        .replace(/[&\\#,+()$~%.'":;*?=<>{}!"#$%&'()*+,.:;<=>?@[\]^_`{|}~]/g, " ")
        .replace(/ /g, "_").replace(/_+/g, '_');
}

function formatJiraTicket(jiraTicket) {
   if (checkJira(jiraTicket)) {
       var jiraTicketTypeString = "";
       document.getElementById("formattedTicket").innerHTML = jiraTicketTypeString;
       jiraTicketTypeString = jiraTicketTypeString.concat(jiraTicketTypeValue(jiraTicket.ticketType.trim().substring(
           jiraTicket.ticketType.trim().lastIndexOf('"> ') + 3,
           jiraTicket.ticketType.trim().lastIndexOf("<span")
           ).trim().toLowerCase()
       ), "/", jiraTicket.ticketKey, "--", replaceUmlaute(jiraTicket.ticketSum.substr(0, jiraTicket.ticketSum.indexOf('<span'))));
       document.getElementById("formattedTicket").innerHTML = jiraTicketTypeString;
       var dummy = document.createElement("textarea");
       document.body.appendChild(dummy);
       dummy.value = jiraTicketTypeString;
       dummy.select();
       document.execCommand("copy");
       document.body.removeChild(dummy);
   }
}

document.getElementById("button").addEventListener("click", format);
document.getElementById("button").addEventListener("keypress", format);

function format() {
    var unformattedTicket = document.getElementById("unformattedTicket").value;
    var formattedTicket = replaceUmlaute(unformattedTicket);
    document.getElementById("formattedTicket").innerHTML = formattedTicket;
    var dummy = document.createElement("textarea");
    document.body.appendChild(dummy);
    dummy.value = formattedTicket;
    dummy.select();
    document.execCommand("copy");
    document.body.removeChild(dummy);
}

